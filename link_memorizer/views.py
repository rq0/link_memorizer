import datetime
from uuid import uuid4

import redis
import tldextract
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView


class VisitedLinks(APIView):
    """
    Ресурс служит для передачи в сервис массива ссылок.
    Временем их посещения считается время получения запроса сервисом.
    """

    def __init__(self):
        super(VisitedLinks, self).__init__()
        self.redis_client = redis.Redis()

    def post(self, request):
        if 'links' not in request.data:
            return Response({"status": "Param links is missed"}, status=status.HTTP_400_BAD_REQUEST)
        if not request.data['links']:
            return Response({"status": "Links cannot be empty."}, status=status.HTTP_400_BAD_REQUEST)
        if not isinstance(request.data['links'], list):
            return Response({"status": "Links is not a list."}, status=status.HTTP_400_BAD_REQUEST)

        request_timestamp = int(datetime.datetime.now().timestamp())

        for link in request.data['links']:
            if not isinstance(link, str):
                return Response({"status": "Link is not a string."}, status=status.HTTP_400_BAD_REQUEST)

            domain = tldextract.extract(link).registered_domain
            domain_rank = self.get_domain_rank(domain)
            self.redis_client.zadd('visits', {"%s:%s" % (domain_rank, uuid4()): request_timestamp})

        return Response({"status": "ok"}, status=status.HTTP_200_OK)

    def get_domain_rank(self, domain):
        domain_rank = self.redis_client.zrank('domains', domain)
        if domain_rank is None:
            domains_length = self.redis_client.zcard('domains')
            self.redis_client.zadd('domains', {domain: domains_length})
            domain_rank = self.redis_client.zrank('domains', domain)
        return domain_rank


class VisitedDomains(APIView):
    """
    Ресурс служит для получения списка уникальных доменов,
    посещенных за переданный интервал времени.
    """

    def __init__(self):
        super(VisitedDomains, self).__init__()
        self.redis_client = redis.Redis()

    def get(self, request):
        if 'from' not in request.query_params:
            return Response({"status": "Param from was not given"}, status=status.HTTP_400_BAD_REQUEST)
        if 'to' not in request.query_params:
            return Response({"status": "Param to was not given"}, status=status.HTTP_400_BAD_REQUEST)

        ts_from = request.query_params['from']
        if not ts_from.isnumeric():
            return Response({"status": "Param from is not int"}, status=status.HTTP_400_BAD_REQUEST)

        ts_to = request.query_params['to']
        if not ts_to.isnumeric():
            return Response({"status": "Param to is not int"}, status=status.HTTP_400_BAD_REQUEST)

        visit_list = self.redis_client.zrangebyscore('visits', ts_from, ts_to)
        domain_rank_set = set()

        for visit in visit_list:
            domain_rank = visit.decode("utf-8").split(":")[0]
            domain_rank_set.add(domain_rank)

        domain_list = []
        for domain_rank in list(domain_rank_set):
            domain = self.redis_client.zrangebyscore('domains', domain_rank, domain_rank)
            domain_list.append(domain)

        return Response({
            "domains": domain_list,
            "status": "ok"
        },
            status=status.HTTP_200_OK)

from django.db import models


class Visit(models.Model):
    link = models.CharField(verbose_name="Адрес ссылки", max_length=1024)
    date_timestamp = models.FloatField(verbose_name="Дата посещения")

    def __str__(self):
        return self.link

    class Meta:
        verbose_name = 'Посещение'
        verbose_name_plural = 'Посещения'

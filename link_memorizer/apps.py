from django.apps import AppConfig


class LinkMemorizerConfig(AppConfig):
    name = 'link_memorizer'

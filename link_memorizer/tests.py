import datetime

import redis
from django.urls import reverse
from freezegun import freeze_time
from rest_framework import status
from rest_framework.test import APITestCase


class VisitedLinksTest(APITestCase):
    @staticmethod
    def get_visit_list(request_timestamp, response_timestamp):
        redis_client = redis.Redis()
        visits = redis_client.zrangebyscore(
            'visits',
            request_timestamp,
            response_timestamp)
        redis_client.delete('visits')
        return visits

    @freeze_time("2012-01-01 00:00:00")
    def test_create_links(self):
        """Ensure we can create a new link objects"""
        url = reverse('visited_links')
        data = {
            "links": [
                "https://ya.ru",
                "https://ya.ru?q=123",
                "funbox.ru",
                "https://stackoverflow.com/questions/11828270/how-to-exit-the-vim-editor"
            ]
        }
        request_timestamp = int(datetime.datetime.now().timestamp())
        response = self.client.post(url, data, format='json')
        response_timestamp = int(datetime.datetime.now().timestamp())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['status'], 'ok')

        self.assertEqual(
            len(
                self.get_visit_list(
                    request_timestamp,
                    response_timestamp)),
            4)

    @freeze_time("2012-01-01 00:01:00")
    def test_create_links_without_link_param(self):
        """Ensure we can create a new link objects"""
        url = reverse('visited_links')
        request_timestamp = int(datetime.datetime.now().timestamp())
        response = self.client.post(url, {}, format='json')
        response_timestamp = int(datetime.datetime.now().timestamp())

        self.assertEqual(response.data['status'], "Param links is missed")
        self.assertEqual(
            len(
                self.get_visit_list(
                    request_timestamp,
                    response_timestamp)),
            0)

    @freeze_time("2012-01-01 00:02:00")
    def test_create_links_with_empty_link_param(self):
        """Ensure we can create a new link objects"""
        url = reverse('visited_links')
        data = {
            "links": []
        }
        request_timestamp = int(datetime.datetime.now().timestamp())
        response = self.client.post(url, data, format='json')
        response_timestamp = int(datetime.datetime.now().timestamp())

        self.assertEqual(response.data['status'], "Links cannot be empty.")
        self.assertEqual(
            len(
                self.get_visit_list(
                    request_timestamp,
                    response_timestamp)),
            0)

    @freeze_time("2012-01-01 00:03:00")
    def test_create_links_with_dict_link(self):
        """Ensure we can create a new link objects"""
        url = reverse('visited_links')
        data = {
            "links": {
                'url': 'ya.ru'
            }
        }
        request_timestamp = int(datetime.datetime.now().timestamp())
        response = self.client.post(url, data, format='json')
        response_timestamp = int(datetime.datetime.now().timestamp())
        self.assertEqual(response.data['status'], "Links is not a list.")
        self.assertEqual(
            len(
                self.get_visit_list(
                    request_timestamp,
                    response_timestamp)),
            0)


class VisitedDomainsTest(APITestCase):
    def create_visits(self):
        url = reverse('visited_links')
        data = {
            "links": [
                "https://ya.ru",
                "https://ya.ru?q=123",
                "funbox.ru",
                "https://stackoverflow.com/questions/11828270/how-to-exit-the-vim-editor"
            ]
        }
        request_timestamp = int(datetime.datetime.now().timestamp())
        self.client.post(url, data, format='json')
        response_timestamp = int(datetime.datetime.now().timestamp())
        return request_timestamp, response_timestamp

    def test_get_domains(self):
        """Ensure we get only 3 domains"""
        date_from, date_to = self.create_visits()

        url = "%s?from=%s&to=%s" % (reverse('visited_domains'), date_from, date_to)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['status'], 'ok')
        self.assertEqual(len(response.data['domains']), 3)

    def test_get_domains_without_from(self):
        self.create_visits()

        url = "%s" % reverse('visited_domains')
        response = self.client.get(url)
        self.assertEqual(response.data['status'], "Param from was not given")
        self.assertNotIn('domains', response.data)

    def test_get_domains_with_decimal_from(self):
        date_from, date_to = self.create_visits()

        url = "%s?from=%s&to=%s" % (reverse('visited_domains'), date_from - 0.666, date_to)
        response = self.client.get(url)
        self.assertEqual(response.data['status'], "Param from is not int")
        self.assertNotIn('domains', response.data)

    def test_get_domains_without_to(self):
        date_from, date_to = self.create_visits()

        url = "%s?from=%s" % (reverse('visited_domains'), date_from)
        response = self.client.get(url)
        self.assertEqual(response.data['status'], "Param to was not given")
        self.assertNotIn('domains', response.data)

    def test_get_domains_with_decimal_to(self):
        date_from, date_to = self.create_visits()

        url = "%s?from=%s&to=%s" % (reverse('visited_domains'), date_from, date_to - 0.666)
        response = self.client.get(url)
        self.assertEqual(response.data['status'], "Param to is not int")
        self.assertNotIn('domains', response.data)

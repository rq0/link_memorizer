## Web-приложение для простого учета посещенных (неважно, как, кем и когда) ссылок. ##
- Python3.7
- Django
- djangorestframework
- tldextract

### Прогон тестов ###
```
pipenv install
pipenv shell
python manage.py test
```

### Запуск ###
```
pipenv install
pipenv shell
python manage.py runserver
```
**Ресурс загрузки посещений:**

*POST /visited_links*
```
{
  "links": [
    "https://ya.ru",
    "https://ya.ru?q=123",
    "funbox.ru",
    "https://stackoverflow.com/questions/11828270/how-to-exit-the-vim-editor"
  ]
}
```

```json
{
  "status": "ok"
}
```

**Ресурс получения статистики:**

*GET /visited_domains?from=1545221231&to=1545217638*

```json
{
  "domains": [
    "ya.ru",
    "funbox.ru",
    "stackoverflow.com"
  ],
  "status": "ok"
}
```

1) Первый ресурс служит для передачи в сервис массива ссылок в POST-запросе. Временем их посещения считается время получения запроса сервисом.

2) Второй ресурс служит для получения GET-запросом списка уникальных доменов,
посещенных за переданный интервал времени.

3) Поле status ответа служит для передачи любых возникающих при обработке запроса
ошибок.
